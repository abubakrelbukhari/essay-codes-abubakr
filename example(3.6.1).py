# File name: example(3.6.2).py
# Author: Abu bakr Elbukhari
# Date created: April/2016
# Using ultraspherical method to compute approximate solution to the ODE u'(x)+u(x)/(ax^2 +1)= 0   , u(-1)=1

# Begin code


from scipy.linalg import toeplitz
from scipy.linalg import hankel
from scipy import linalg
from numpy.linalg import solve
import numpy as np
from numpy  import * 
n = 80

# Function to generate the conversion operator matrix S
def convertmatr3(n):
    "Make differentiation matrix of size nxn"
    S0 = np.zeros(shape=(n,n))

    for j in range(n-2):
        S0[j][j+2] = -0.5
        for j in range(n):
            S0[j][j] = 0.5
            if  S0[1][1] == 0.5:
                S0[0][0]=1
    return S0
    
S0 = convertmatr3(n)
print "S0=", S0
 
# function return an nxn differentiation  matrix for given input n
def diffmatr3(n):
    "Make differentiation matrix of size nxn"
    D1 = np.zeros(shape=(n,n))
    for k in range(n-1):
        D1[k][k+1] = k+1
    return D1
D1 = diffmatr3(n)
print "D1 = ", D1

# Function of 1/(ax^2 +1) we have been using in multiplication
a = 5 * 10**0
def f(x): return (a * x**2 +1)**(-1)

# Function of chebyshev coefficients of a(x)=\sum_{k=0}^4 c T_k(x)
def chebcoeffs1(f, n):
    x = np.zeros(shape=(n,1))
    nn = np.zeros(shape=(n,1))
    for k in range(n):
        x[k] = np.cos(k*np.pi/(n-1))
        nn[k] = k
    theta = np.transpose(np.arccos(x))
    T = np.cos(theta*nn)
    c = linalg.solve(T, f(x))
    return c
c = chebcoeffs1(f, n)
print "c=", c

# Function return multiplication operator (toeplitz and hankel matrices)
def multmatt(n, f):
    c = chebcoeffs1(f, n)
    c[0] = 2*c[0]
    T = toeplitz(c)
    c[0] = 0
    H = hankel(c)
    for k in range(n):
        H[0][k] = 0
    M = .5*(T + H)
    return M
M = multmatt(n, f)
print "M=", M
#print "T=", T
#print "T + H=", T + H
#print "H=", H



A = D1 + np.dot(S0,M)
print "A=", A

# Add the boundary conditions
for k in range(0,n,2):
    A[n-1,k] = 1;
for k in range(1,n,2):
    A[n-1,k] = -1;
print "A = ", A

# righthand side and applying the boundary condition U(-1)=1
rhs = np.zeros(shape=(n,1))
rhs[n-1] = 1
print "rhs=", rhs

#  solve the linear system
u = solve(A, rhs)
print "u = ", u

# Function return f(x) by using the chebyshev coefficients u
def clenshaw(u, x):
    bk1 = 0 * x
    bk2 = bk1
    n = len(u)
    for k in range(n-1):
        bk = u[n-1-k] + 2*x*bk1 - bk2
        bk2 = bk1
        bk1 = bk
    y = u[0] + x*bk1 - bk2
    return y
x = np.linspace(-1,1,101)
#x = np.array([-np.cos(np.pi*j/n) for j in range(n+1)])
ux = clenshaw(u, x)
print "ux=", ux

#plot the solution
s = np.zeros(shape=(101,1))
for k in range(101):
    s[k] = np.exp(-(math.atan(np.sqrt(a)*x[k])+ math.atan(np.sqrt(a)))/ np.sqrt(a))

import matplotlib.pyplot as plt
plt.plot(x, ux,'bs',markersize=7,label='Approximate')
plt.plot(x, s,'r^-',markersize=7,label='Exact')
plt.xlim(-1.0, 1.0)
plt.legend(fontsize = 10) 
plt.title(' Exact and approximated solution plot',fontsize = 17)
plt.xlabel('x',fontsize = 17)
plt.ylabel(' u(x)',fontsize = 17)
plt.show()
