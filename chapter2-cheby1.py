# File name: chapter2-cheby1.py
# Author: Abu bakr Elbukhari
# Date created: April/2016

# Plot of chebyshev polynomials of the first kind

# Begin code

import numpy as np
import matplotlib.pyplot as plt
#n =5
x = np.linspace(-1,1,101)

#x = np.array([-np.cos(np.pi*j/n) for j in range(n+1)]) # Chebyshev points
T0 = x/x
T1 = x
T2 = 2*x**2 -1
T3 = 4*x**3 -3*x
T4 = 8 * x**4 -8*x**2 + 1
T5 = 16 * x**5 - 20*x**3 + 5 *x
plt.xlim(-1.0, 1.0)
plt.ylim(-1.0, 1.0)
plt.plot(x, T0,markersize=7,label='$T_0(x)$')
plt.plot(x, T1,markersize=7,label='$T_1(x)$')
plt.plot(x, T2,markersize=7,label='$T_2(x)$')
plt.plot(x, T3,markersize=7,label='$T_3(x)$')
plt.plot(x, T4,markersize=7,label='$T_4(x)$')
plt.plot(x, T5,markersize=7,label='$T_5(x)$')
plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=6,
           ncol=8, mode="expand", borderaxespad=0.)
plt.xlabel('x ')
plt.ylabel('$T_k(x)$ ')
plt.grid()
plt.show()
