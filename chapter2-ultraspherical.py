# File name: chapter2-ultraspherical.py
# Author: Abu bakr Elbukhari
# Date created: April/2016

# Plotting of first 4 ultraspherical polynomials when lambda=1

# Begin code

import numpy as np
import matplotlib.pyplot as plt
x = np.linspace(-1,1,101)
C0 = x/x
C1 = 2*(x)
C2 = 4*x**2 -1
C3 = 8* (x**3) -4*(x)
plt.xlim(-1.0, 1.0)
plt.ylim(-3.0, 4.0)
plot1 = plt.plot(x, C0,markersize=7,label='$C_0^{(1)}(x)$')
plot2 = plt.plot(x, C1,markersize=7,label='$C_1^{(1)}(x)$')
plot3 = plt.plot(x, C2,markersize=7,label='$C_2^{(1)}(x)$')
plot4 = plt.plot(x, C3,markersize=7,label='$C_3^{(1)}(x)$')
plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=4,
           ncol=8, mode="expand", borderaxespad=0.)
plt.xlabel('x')
plt.ylabel('$C_k(x)$')
plt.grid()
plt.show()


