# File name: convergence-example(3.4.1).py
# Author: Abu bakr Elbukhari
# Date created: April/2016

# Begin code

# solving and computing the converfence of u'(x)+u(x)= 1   , u(+1)=0 
from scipy import linalg
from numpy import linalg as LA
from scipy import random, linalg, dot, diag, all, allclose
from numpy.linalg import solve
import numpy as np
import math
import matplotlib.pyplot as plt

listn=[]
listnnn1=[]
for n in range(1,20):

    # Function to generate the differentiation operator matrix S
    def diffmat5_v2(n,l):
        "Make differentiation matrix of size 5x5"

        D1 = np.zeros(shape=(n,n))
        for k in range(0,n-l):
            D1[k][k+l] = 2**(l-1) * math.factorial(l-1) * (l+k)
        return D1
    l=1                                                          #l is the parameter lambda
    D1 = diffmat5_v2(n,l)
    #print  "D1 = ",D1

    # Function to generate the conversion operator matrix S
    def convertmat(n):
        "Make differentiation matrix of size nxn"
        S0 = np.zeros(shape=(n,n))

        for j in range(n-2):
            S0[j][j+2] = -0.5
            for j in range(n):
                S0[j][j] = 0.5
                if  S0[1][1] == 0.5:
                    S0[0][0]=1
        return S0
    S0 = convertmat(n)
    #print "S0=", S0

    # Now we can add the matrices D1 + S0 
    A1 = D1 + S0 
    #print "A1 = ", A1

    # Add the boundary conditions
    for k in range(0,n):
        A1[n-1,k] = 1;
    #print "A1 = ", A1

    # Maker the righthand side
    rhs = np.zeros(shape=(n,1))
    rhs[0] = 1
    #print "rhs=", rhs
    
    # Solve thelinear system
    u = solve(A1, rhs)
    #print "u = ", u

    # Function return f(x) by using the chebyshev coefficients u
    def clenshaw(u, x):
        bk1 = 0 * x
        bk2 = bk1
        n = len(u)
        for k in range(n-1):
            bk = u[n-1-k] + 2*x*bk1 - bk2
            bk2 = bk1
            bk1 = bk
        y = u[0] + x*bk1 - bk2
        return y
    x = np.linspace(-1,1,101)
    x = np.array([-np.cos(np.pi*j/n) for j in range(n+1)])
    ux = clenshaw(u, x)
    
    # analytic solution
    v = 1 - np.exp(1-x)
    
    # analytic minus exact  solution
    difr= ux-v
    nn = []
    
    # Computiog the Error ||v-ux||_2 and logs10 
    norm2=math.log10(LA.norm(difr ,2))
    nn.append(norm2)
    nn1= np.array(norm2)
    
    #print nn1
    listn.append(n)
    listnnn1.append(float(nn1))

# Plot the convergence    
plt.plot( listn,listnnn1,'r-o',markersize=7)
#plt.plot( x,v,'o')
#plt.plot( x,ux)
plt.xlabel('N',fontsize = 13)
plt.ylabel('log10||u_{approix}-u_{exact}||_2$',fontsize = 13)
plt.show()
