# File name: analytical-solutions.py
# Author: Abu bakr Elbukhari
# Date created: April/2016


# analytical solution of u'(x)+u(x)= 1   , u(+1)=0

# Begin code

import matplotlib.pyplot as plt
import numpy as np
#n=101
#x = np.array([-np.cos(np.pi*j/n) for j in range(n+1)]) # Chebyshev points
x = np.linspace(-1,1,101)
v = 1 - np.exp(1-x)
plt.plot(x, v,'-^',markersize=7,label='Exact')
plt.xlim(-1.0, 1.0)
plt.legend(fontsize = 10) 
plt.title(' Exact solution plot')
plt.xlabel('x values')
plt.ylabel('Function coefficients of exact solution ')
plt.show()

# analytical solution of u''(x)+u(x)= 1   , u(+1\-1)=0

# Begin code

import matplotlib.pyplot as plt
import numpy as np
import math
#n=101
#x = np.array([-np.cos(np.pi*j/n) for j in range(n+1)]) # Chebyshev points
x = np.linspace(-1,1,101)
v = 1 - np.cos(x)/np.cos(1)
plt.plot(x, v,'g-^',markersize=7,label='Exact')
plt.xlim(-1.0, 1.0)
plt.legend(fontsize = 10) 
plt.title(' Exact vs approximated solution plot')
plt.xlabel('x values')
plt.ylabel('Function coefficients of exact and approximated solution ')
plt.show()

# The analytical solution of u'(x)+u(x)/(ax^2 +1)= 1   , u(-1)=1,  a = 5 * (10)**1

# Begin code

import matplotlib.pyplot as plt
import numpy as np
import math
a = 5 * (10)**1
x = np.linspace(-1,1,101)
#x = np.array([-np.cos(np.pi*j/n) for j in range(n+1)])
s = np.zeros(shape=(101,1))
for k in range(101):
    s[k] = np.exp(-(math.atan(np.sqrt(a)*x[k])+ math.atan(np.sqrt(a)))/ np.sqrt(a))
plt.plot(x, s,'g-',markersize=7,label='Exact')
plt.xlim(-1.0, 1.0)
plt.legend(fontsize = 10) 
plt.title(' Exact vs approximated solution plot')
plt.xlabel('x values')
plt.ylabel('Function coefficients of exact and approximated solution ')
plt.show()

# The analytical solution of  e*u''(x)-x*u(x)= 0   , u(1)=e^(1/3), u(-1)=-e^(1/3),  e = 5* 10^-3

# Begin code

import matplotlib.pyplot as plt
import numpy as np
import scipy.special
import math
e = 5 * (10)**-3
x = np.linspace(-1,1,101)
s = scipy.special.airy(e**(-1./3)*x)[0]
plt.plot(x, s,'r-')
plt.show()
