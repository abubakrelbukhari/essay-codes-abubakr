# File name: chapter2-monomial-basis.py
# Author: Abu bakr Elbukhari
# Date created: April/2016

# Plotting of first 13 monomials basis

# Begin code

import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(-1,1,101)
y0 = x/x
y1 = x
y2 = x**2
y3 = x**3
y4 = x**4
y5 = x**5
y6 = x**6
y7 = x**7
y8 = x**8
y9 = x**9
y10 = x**10
y11 = x**11
y12 = x**12
y13 = x**13
plt.xlim(0, 1)
plt.ylim(0, 1)
plot1 = plt.plot(x, y0)
plot2 = plt.plot(x, y1)
plot3 = plt.plot(x, y2)
plot4 = plt.plot(x, y3)
plot1 = plt.plot(x,y4)
plot2 = plt.plot(x, y5)
plot3 = plt.plot(x, y6)
plot4 = plt.plot(x, y7)
plot3 = plt.plot(x, y8)
plot4 = plt.plot(x, y9)
plot1 = plt.plot(x, y10)
plot2 = plt.plot(x, y11)
plot3 = plt.plot(x, y12)
plot4 = plt.plot(x, y13)
plt.title('Standrd monomials basis functions')
plt.xlabel('<..x..>')
plt.ylabel('monomials basis through 0 to 13')
plt.grid()
plt.show()
