# File name: convergence-example(3.6.2).py
# Author: Abu bakr Elbukhari
# Date created: April/2016

# Begin code

# solving and computing the converfence of the airy ODE  e*u''(x)-x*u(x)= 0   , u(1)=e^(1/3)    , u(-1)=-e^(1/3)
from scipy.linalg import toeplitz
from scipy.linalg import hankel
from scipy import linalg
from numpy import linalg as LA
from scipy import random, linalg, dot, diag, all, allclose
from numpy.linalg import solve
import scipy.special
import numpy as np
from numpy  import * 
import math
import matplotlib.pyplot as plt

ln1 = []
ln12 = []

for n in range(2,80):

    # Function to generate the differentiation operator matrix S
    def diffmat5_v2(n,l):
        "Make differentiation matrix of size 5x5"
        D2 = np.zeros(shape=(n,n))
        for k in range(0,n-l):
            D2[k][k+l] = 2**(l-1) * math.factorial(l-1) * (l+k)
        return D2
    l=2                                                          #y is the parameter lambda
    D2 = diffmat5_v2(n,l)
    #print  "D2 = ",D2

    # Function to generate the conversion operator matrix S
    def convertmat4(n):
        "Make differentiation matrix of size nxn"
        S0 = np.zeros(shape=(n,n))
        for j in range(n-2):
            S0[j][j+2] = -0.5
            for j in range(n):
                S0[j][j] = 0.5
                if  S0[1][1] == 0.5:
                    S0[0][0]=1
        return S0
    S0 = convertmat4(n)
    #print "S0=", S0

    # Function to generate the conversion operator matrix S
    def convertmat(n,l):
        "Make converison matrix of size nxn"
        S1 = np.zeros(shape=(n,n))
        for s in range(2,n+1):
            for k in range(1,n-2):
                S1[0][0+2] = -l/float(l+1+1)
                S1[k][k+2] = -l/float(l+k+2)
                #for r in range(0,n-1):
                for k in range(n-1):
                    S1[k][k] = l/float(l+k)
                    S1[k+1][k+1] = l/float(l+k+1)
        return S1
    l=1    
    S1 = convertmat(n,l)
    #print  "S1=",S1

    # Function -x we have been using in multiplication
    def f(x): return -x

    # Function of chebyshev coefficients of a(x)=\sum_{k=0}^4 c T_k(x)
    def chebcoeffs12(f, n):
        x = np.zeros(shape=(n,1))
        nn = np.zeros(shape=(n,1))
        for k in range(n):
            x[k] = np.cos(k*np.pi/(n-1))
            nn[k] = k
            theta = np.transpose(np.arccos(x))
            T = np.cos(theta*nn)
        c = linalg.solve(T, f(x))
        return c
    c = chebcoeffs12(f, n)
    #print "c=", c

# Function to add the teoplites and hankel matrices    
    def multmatt12(n, f):
        c = chebcoeffs12(f, n)
        c[0] = 2*c[0]
        T = toeplitz(c)
        c[0] = 0
        H = hankel(c)
        for k in range(n):
            H[0][k] = 0
        M = .5*(T + H)
        return M
    M = multmatt12(n, f)
    #print "M=", M


    # Define operator coefficient e = 5* 10^-3
    e =5*(10)**-3
    S = np.dot(S1, S0)
    A = (e*D2) +  np.dot(S,M)
    #print "A=", A

    # Add the boundary conditions
    for k in range(0,n):
        A[n-1,k] = 1;
    for k in range(1,n,2):
        A[n-2,k] = -1;
    for k in range(0,n,2):
        A[n-2,k] = 1;
    #print "A = ", A

    # righthand side and applying the boundary condition U(-1)=
    rhs = np.zeros(shape=(n,1))
    a1 = scipy.special.airy(e**(-1./3))
    a2 = scipy.special.airy(-e**(-1./3))
    rhs[n-1] = a1[0]
    rhs[n-2] = a2[0]
    #print "rhs=", rhs

    # solving the system
    u = solve(A, rhs)
    #print "u = ", u

    # Function return f(x) by using the chebyshev coefficients u
    def clenshaw(u, x):
        bk1 = 0 * x
        bk2 = bk1
        n = len(u)
        for k in range(n-1):
            bk = u[n-1-k] + 2*x*bk1 - bk2
            bk2 = bk1
            bk1 = bk
        y = u[0] + x*bk1 - bk2
        return y
    #x = np.array([-np.cos(np.pi*j/n) for j in range(n+1)])
    x = np.linspace(-1,1,101)
    ux = clenshaw(u, x)
    #print "ux=", ux
    #print "ux=", ux
    # analytic solution
    s = scipy.special.airy(e**(-1./3)*x)[0]
    er = np.zeros(shape=(101,1))
    for k in range(101):
        er[k] = np.abs(s[k] - ux[k])
    nm=np.log10(LA.norm(er, np.inf))
    nm1=[]
    nm1.append(nm)
    op=np.array(nm1)
    ln1.append(n)
    ln12.append(float(op))
#plot the solution and the convergence
plt.plot(ln1, ln12,'ro-')
#plt.plot(x, ux,'*')
#plt.plot(x, s,'g-')
plt.xlabel('N',fontsize = 17)
plt.ylabel('log10$||u_{approix}-u_{exact}||_\infty$',fontsize = 17)
plt.show()
