# File name: example(3.4.2).py
# Author: Abu bakr Elbukhari
# Date created: April/2016
# Using ultraspherical method to compute approximate solution to the ODE u''(x)+u(x)= 1 ,  u(+1\-1)=0

# Begin code

from scipy import linalg
from numpy.linalg import solve
import numpy as np
from numpy  import *
import matplotlib.pyplot as plt

n = 40
# Function to generate the differentiation operator matrix S
def diffmat5_v2(n,l):
    "Make differentiation matrix of size 5x5"
    
    D2 = np.zeros(shape=(n,n))
    for k in range(0,n-l):
        D2[k][k+l] = 2**(l-1) * math.factorial(l-1) * (l+k)
    return D2

l=2                                                          #y is the parameter lambda
D2 = diffmat5_v2(n,l)
print  "D2 = ",D2

# Function to generate the conversion operator matrix S
def convertmat0(n):
    "Make differentiation matrix of size nxn"
    S0 = np.zeros(shape=(n,n))

    for j in range(n-2):
        S0[j][j+2] = -0.5
        for j in range(n):
            S0[j][j] = 0.5
            if  S0[1][1] == 0.5:
                S0[0][0]=1
    return S0
    
    

S0 = convertmat0(n)
print "S0=", S0

# Function to generate the conversion operator matrix S
def convertmat(n,l):
    "Make converison matrix of size nxn"
    S1 = np.zeros(shape=(n,n))
    for s in range(2,n+1):
        for k in range(1,n-2):
            S1[0][0+2] = -l/float(l+1+1)
            S1[k][k+2] = -l/float(l+k+2)
            #for r in range(0,n-1):
            for k in range(n-1):
                S1[k][k] = l/float(l+k)
                S1[k+1][k+1] = l/float(l+k+1)
    return S1
l=1    

S1 = convertmat(n,l)
print  "S1=",S1


# Now we can add the matrices D2 + (S0 * S1)
A2 = D2 + np.dot(S1,S0) 
print "A2 = ", A2

# Add the boundary conditions
for k in range(0,n):
    A2[n-2,k] = 1;
for k in range(0,n,2):
    A2[n-1,k] = 1;
for k in range(1,n,2):
    A2[n-1,k] = -1;
print "A2 = ", A2

# Maker the righthand side
rhs = np.zeros(shape=(n,1))
rhs[0] = 1
print "rhs=", rhs

# Solve thelinear system
u = solve(A2, rhs)
print "u = ", u

# Function return f(x) by using the chebyshev coefficients u
def clenshaw(u, r):
    bk1 = 0 * r
    bk2 = bk1
    n = len(u)
    for k in range(n-1):
        bk = u[n-1-k] + 2*r*bk1 - bk2
        bk2 = bk1
        bk1 = bk
    y = u[0] + r*bk1 - bk2
    return y
r = np.linspace(-1,1,101)
#x = np.array([-np.cos(np.pi*j/n) for j in range(n+1)])
ux = clenshaw(u, r)
print "ux=", ux

#The exact solution
x = np.linspace(-1,1,101)
v = 1 - np.cos(x)/np.cos(1)   
plt.plot(x, v,'r^',markersize=7,label='Exact')
plt.plot(r, ux,'bx',markersize=7,label='Approximate, N=40')
plt.plot(r, uux,'-',markersize=7,label='Approximate, N=3')
plt.xlim(-1.0, 1.0)
plt.legend(fontsize = 10) 
plt.title(' Exact vs approximated solution plot',fontsize = 17)
plt.xlabel('x values',fontsize = 15)
plt.ylabel('u(x)',fontsize = 13)
plt.show()
