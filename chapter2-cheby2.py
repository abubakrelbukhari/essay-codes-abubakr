# File name: chapter2-cheby2.py
# Author: Abu bakr Elbukhari
# Date created: April/2016

# Plot of chebyshev polynomials of second kind

# Begin code

import numpy as np
import matplotlib.pyplot as plt
x = np.linspace(-1,1,101)
U0 = x/x
U1 = 2*x
U2 = 4*x**2 -1
U3 = 8*x**3 -4*x
U4 = 16 * x**4 -12*x**2 + 1
U5 = 32 * x**5 - 32*x**3 + 6 *x
plt.xlim(-1.0, 1.0)
plt.ylim(-1.5, 1.5)
plot1 = plt.plot(x, U0,label='$U_0(x)$')
plot2 = plt.plot(x, U1,label='$U_1(x)$')
plot3 = plt.plot(x, U2,label='$U_2(x)$')
plot4 = plt.plot(x, U3,label='$U_3(x)$')
plot5 = plt.plot(x, U4,label='$U_4(x)$')
plot6 =plt.plot(x, U5,label='$U_5(x)$')
plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=6,
           ncol=8, mode="expand", borderaxespad=0.)
plt.xlabel('x ')
plt.ylabel('$U_k(x)$ ')
plt.grid()
plt.show()
